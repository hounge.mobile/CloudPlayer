package com.houngedevelopment.cloudplayer.fragments;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.adapter.HistoryTrackAdapter;
import com.bk.lrandom.yourcloudplayer.business.Ultils;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.dals.HistoryDal;
import com.bk.lrandom.yourcloudplayer.models.Track;

import android.app.Activity;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ExpandableListView;
import android.widget.ListView;

public class HistoryFragment extends Fragment {
	Track track;
	ArrayList<Track> tracks;
	ListView list;
	Context context;
	HistoryTrackAdapter adapter;

	public static final HistoryFragment newInstance() {
		HistoryFragment fragment = new HistoryFragment();
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.context = activity;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.track_context_menu, menu);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.listview_container_layout,
				container, false);
		HistoryDal historyDal = new HistoryDal(getActivity());
		historyDal.getConnect();
		tracks = historyDal.getTracks();
		adapter = new HistoryTrackAdapter(context,
				R.layout.track_history_item_layout, tracks);
		list = (ListView) view.findViewById(R.id.list);
		list.setAdapter(adapter);
		historyDal.close();
		return view;
	}

	public void refreshUI() {
		HistoryDal historyDal = new HistoryDal(getActivity());
		historyDal.getConnect();
		tracks = historyDal.getTracks();
		adapter = new HistoryTrackAdapter(context,
				R.layout.track_history_item_layout, tracks);
		list.setAdapter(adapter);
		list.invalidateViews();
	}
}