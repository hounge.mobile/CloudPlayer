package com.houngedevelopment.cloudplayer.fragments;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.interfaces.CustomPromptDialogFragmentComunicator;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CustomPromptDialogFragment extends DialogFragment {
	CustomPromptDialogFragmentComunicator listener;
	EditText inputText = null;

	public static CustomPromptDialogFragment newInstance(String title) {
		CustomPromptDialogFragment frag = new CustomPromptDialogFragment();
		Bundle args = new Bundle();
		args.putString("title", title);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		listener = (CustomPromptDialogFragmentComunicator) activity;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("title");
		// Set an EditText view to get user input
		inputText = new EditText(getActivity());
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(inputText)
				.setTitle(title)
				.setPositiveButton(R.string.ok_label, null)
				.setNegativeButton(R.string.cancel_label,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
							}
						});
		AlertDialog promptDialog = builder.create();
		promptDialog.show();
		promptDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
				new View.OnClickListener() {
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (inputText.getText().length() != 0) {
							listener.getInputTextValue(inputText.getText()
									.toString());
							dismiss();
						}
					}
				});
		return promptDialog;
	}
}
