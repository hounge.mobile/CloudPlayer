package com.houngedevelopment.cloudplayer.fragments;

import com.bk.lrandom.yourcloudplayer.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LyricsFragment extends Fragment {
	public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
	public static final String LYRICS_KEY = "LYRICS_KEY";
	private String content;
	int trackIndex = 0;

	public static final LyricsFragment newInstance() {
		// TODO Auto-generated constructor stub
		LyricsFragment fragment = new LyricsFragment();
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.lyrics_fragment_layout,
				container, false);
		if (getArguments() != null) {
			content = getArguments().getString(LYRICS_KEY);
			if (content == null) {
				content = "";
			}
			TextView contentView = (TextView) view.findViewById(R.id.lyrics);
			contentView.setText(Html.fromHtml(content).toString());
		}
		return view;
	}

	public void setContent(String content) {
		this.content = content;
		if (getView() != null) {
			TextView contentView = (TextView) getView().findViewById(
					R.id.lyrics);
			if (content == null) {
				content = "";
			}
			contentView.setText(Html.fromHtml(content).toString());
		}
	}
}
