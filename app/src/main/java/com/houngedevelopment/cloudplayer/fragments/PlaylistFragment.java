package com.houngedevelopment.cloudplayer.fragments;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.TrackActivity;
import com.bk.lrandom.yourcloudplayer.adapter.PlaylistAdapter;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.dals.PlaylistDal;
import com.bk.lrandom.yourcloudplayer.models.Playlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;

public class PlaylistFragment extends Fragment {
	ArrayList<Playlist> playlists;
	PlaylistAdapter playlistAdapter;
	ListView list;

	public static final PlaylistFragment newInstance() {
		PlaylistFragment fragment = new PlaylistFragment();
		return fragment;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.playlist_context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.del_playlist:
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
					getActivity());
			dialogBuilder.setMessage(getResources().getString(
					R.string.confirm_delete_playlist_msg));
			dialogBuilder.setPositiveButton(
					getResources().getString(R.string.ok_label),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							PlaylistDal playlistDal = new PlaylistDal(
									getActivity());
							Playlist removePlaylist = playlists
									.get(info.position);
							int row = playlistDal.removePlayList(removePlaylist
									.getId());
							if (row != 0) {
								Toast toast = Toast.makeText(
										getActivity(),
										removePlaylist.getName()
												+ " "
												+ getActivity()
														.getResources()
														.getString(
																R.string.file_deleted_result_msg),
										Integer.parseInt(getActivity()
												.getResources()
												.getString(
														R.string.toast_time_out)));
								toast.show();
								refreshViewAfterRemoveItem(removePlaylist);
							}
							playlistDal.close();
						}
					});
			dialogBuilder.setNegativeButton(
					getResources().getString(R.string.cancel_label),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			dialogBuilder.create().show();
			break;
		}
		return super.onContextItemSelected(item);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.listview_container_layout,
				container, false);
		if (getArguments() != null) {
			playlists = getArguments().getParcelableArrayList(
					constants.PLAYLISTS_KEY);
			if (playlists != null) {
				playlistAdapter = new PlaylistAdapter(getActivity(),
						R.layout.playlist_item_layout, playlists);
				list = (ListView) view.findViewById(R.id.list);
				list.setAdapter(playlistAdapter);
				list.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getActivity()
								.getApplicationContext(), TrackActivity.class);
						Bundle bundle = new Bundle();
						bundle.putInt(constants.PLAYLIST_ID_KEY,
								playlists.get(arg2).getId());
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
				registerForContextMenu(list);
			}
		}
		return view;
	}

	public void refreshViewAfterAddItem(Playlist playlist) {
		playlists.add(playlist);
		playlistAdapter.notifyDataSetChanged();
	}

	public void refreshViewAfterRemoveItem(Playlist playlist) {
		playlists.remove(playlist);
		playlistAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}
}
