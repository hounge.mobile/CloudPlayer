package com.houngedevelopment.cloudplayer.fragments;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.adapter.TrackInQueueAdapter;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.interfaces.TrackInQueueFragmentComunicator;
import com.bk.lrandom.yourcloudplayer.models.Track;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class TrackInQueueFragment extends Fragment {
	ArrayList<Track> tracks;
	ArrayList<Track> trackInQueueDownloads = new ArrayList<Track>();
	int trackIndex = 0;
	private TrackInQueueFragmentComunicator listener;
	ListView list;
	TrackInQueueAdapter trackAdapter;

	public static final TrackInQueueFragment newInstance() {
		TrackInQueueFragment fragment = new TrackInQueueFragment();
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		listener = (TrackInQueueFragmentComunicator) activity;
	}

	public void setTrackIndex(int trackIndex) {
		this.trackIndex = trackIndex;
		if (trackAdapter != null && list != null) {
			trackAdapter.notifyDataSetChanged();
			list.setSelection(trackIndex);
		}
	}

	public void refreshUI(Track track) {
		tracks.remove(track);
		list.invalidateViews();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.listview_container_layout,
				container, false);
		if (getArguments() != null) {
			trackIndex = getArguments().getInt(constants.TRACK_INDEX_KEY);
			tracks = getArguments()
					.getParcelableArrayList(constants.TRACKS_KEY);
			tracks.get(trackIndex).setSelected(true);
			trackAdapter = new TrackInQueueAdapter(getActivity(),
					R.layout.track_item_layout, tracks);
			list = (ListView) view.findViewById(R.id.list);
			list.setAdapter(trackAdapter);
			list.setSelection(trackIndex);
			list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					list.setSelection(arg2);
					tracks.get(trackIndex).setSelected(false);
					trackIndex = arg2;
					tracks.get(arg2).setSelected(true);
					listener.LoadTrackAndPlay(arg2, tracks, true);
				}
			});
		}
		return view;
	}
}
