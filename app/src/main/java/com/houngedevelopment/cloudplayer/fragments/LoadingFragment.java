package com.houngedevelopment.cloudplayer.fragments;

import com.bk.lrandom.yourcloudplayer.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class LoadingFragment extends Fragment {

	public static final LoadingFragment newInstance() {
		LoadingFragment loading = new LoadingFragment();
		return loading;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.loading_layout, null);
		return view;
	}
}
