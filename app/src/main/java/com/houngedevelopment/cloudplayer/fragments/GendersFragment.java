package com.houngedevelopment.cloudplayer.fragments;

import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.ExploreStreamActivity;
import com.bk.lrandom.yourcloudplayer.adapter.GendersAdapter;
import com.bk.lrandom.yourcloudplayer.business.XMLGenderParser;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.models.Gender;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

public class GendersFragment extends Fragment {
	ArrayList<Gender> gender_list = new ArrayList<Gender>();
	ListView list;
	GendersAdapter adapter;
	String TAG = "GenderFragment";
	ProgressBar loadMorePrg;
	LayoutInflater inflater;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		inflater = (LayoutInflater) activity.getLayoutInflater();
		InputStream is = activity.getResources().openRawResource(R.raw.genders);
		gender_list = XMLGenderParser.parse(is);
	}

	public static final GendersFragment newInstance() {
		// TODO Auto-generated constructor stub
		GendersFragment gendersFragment = new GendersFragment();
		return gendersFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.listview_container_layout, null);
		adapter = new GendersAdapter(getActivity(),
				R.layout.common_item_layout, gender_list);
		list=(ListView)view.findViewById(R.id.list);
		list.setAdapter(adapter);
		list.setOnItemClickListener(onItemClickListener);
		return view;
	}

	private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			 Intent intent = new Intent(getActivity(),
			 ExploreStreamActivity.class);
			 Bundle bundle = new Bundle();
			 bundle.putString(constants.GENDER_KEY,gender_list.get(arg2).getName());
			 intent.putExtras(bundle);
			 startActivity(intent);
		}
	};
}
