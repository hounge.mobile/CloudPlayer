package com.houngedevelopment.cloudplayer.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.PlayerActivity;
import com.bk.lrandom.yourcloudplayer.adapter.TrackAdapter;
import com.bk.lrandom.yourcloudplayer.business.JSONFetchTask;
import com.bk.lrandom.yourcloudplayer.business.Ultils;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.models.Track;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("NewApi")
public class ExploreStreamFragment extends Fragment implements OnScrollListener {
	ArrayList<Track> track_list = new ArrayList<Track>();
	ListView list;
	TrackAdapter adapter;
	String TAG = "ExploreStreamFragment";
	JSONFetchTask jsonFetchTask;
	static InputStream is = null;
	static JSONObject jObj = null;
	static String jsonString = "";
	String query = null, tmpQuery = null;
	int COUNT_ITEM_LOAD_MORE = 10;
	int offset = 0;
	ProgressBar loadMorePrg;
	boolean loadingMore = true;

	public static final ExploreStreamFragment newInstance() {
		// TODO Auto-generated constructor stub
		ExploreStreamFragment fragment = new ExploreStreamFragment();
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.listview_container_layout,
				container, false);
		list = (ListView) view.findViewById(R.id.list);
		query = getResources().getString(R.string.json_get_track)
				+ "&filter=downloadable";
		list = (ListView) view.findViewById(R.id.list);
		adapter = new TrackAdapter(getActivity(),
				R.layout.sc_track_item_layout, track_list);
		LinearLayout footerView = (LinearLayout) inflater.inflate(
				R.layout.footer_loadmore_layout, null);
		loadMorePrg = (ProgressBar) footerView.findViewById(R.id.prgLoadMore);
		list.addFooterView(footerView);
		list.setAdapter(adapter);
		list.setOnScrollListener(this);
		list.setOnItemClickListener(listViewOnClick);

		tmpQuery = query + "&offset=" + offset + "&limit="
				+ COUNT_ITEM_LOAD_MORE;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			new LoadMoreDataTask()
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			new LoadMoreDataTask().execute();
		}
		
		AdView adView = (AdView) view.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice(
						getResources()
								.getString(R.string.admob_test_device_ids))
				.build();
		adView.loadAd(adRequest);
		return view;
	}

	private void parse(JSONObject jsonObj, boolean append) {
		try {
			int id = jsonObj.getInt(Track.TAG_ID);
			String name = jsonObj.getString(Track.TAG_TITLE);
			JSONObject username = jsonObj.getJSONObject(Track.TAG_USER);
			String artist = username.getString(Track.TAG_ARTIST);
			String thumb = jsonObj.getString(Track.TAG_THUMB);
			String path = jsonObj.getString(Track.TAG_SC_PATH) + "?client_id="
					+ getResources().getString(R.string.soundcloud_client_id);
			Log.i(TAG, path);
			String downloadable = jsonObj.getString(Track.TAG_DOWNLOADABLE);
			String ext = jsonObj.getString(Track.TAG_FILE_FORMAT);
			Track track = new Track();
			track.setRemote_id(id);
			track.setTitle(name);
			track.setArtist(artist);
			track.setThumb(thumb);
			track.setPath(path);
			track.setDownloadable(downloadable);
			track.setExtension(ext);
			if (downloadable.equals("true")) {
				String download_url = jsonObj.getString(Track.TAG_DOWNLOAD);
				track.setDownload_url(download_url);
			}
			if (append) {
				track_list.add(track);
			} else {
				track_list.add(0, track);
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.w(TAG, e.toString());
			loadingMore = false;
		}
	}

	private void parseAndAppend(String jsonString) {
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObj = jsonArray.getJSONObject(i);
				parse(jsonObj, true);
			}
			loadingMore = false;
			adapter.notifyDataSetChanged();
			loadMorePrg.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			loadingMore = true;
			loadMorePrg.setVisibility(View.GONE);
		}
	}

	private String feedJson(String pullQuery) {
		try {
			HttpGet httpGet = null;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			if (pullQuery != null && !pullQuery.equalsIgnoreCase("")) {
				httpGet = new HttpGet(pullQuery);
			} else {
				httpGet = new HttpGet(tmpQuery);
			}
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, constants.STREAM_READER_CHARSET), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			jsonString = sb.toString();
			Log.i("JSON_FETCH_TAG", jsonString);
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}
		;
		return jsonString.replaceAll("\\\\'", "'");
	}

	private class LoadMoreDataTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			if (isCancelled()) {
				return null;
			}
			return feedJson(null);
		}

		@Override
		protected void onPostExecute(String result) {
			parseAndAppend(jsonString);
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			loadMorePrg.setVisibility(View.VISIBLE);
			loadingMore = true;
		}

		@Override
		protected void onCancelled() {
			loadingMore = false;
			loadMorePrg.setVisibility(View.GONE);
		}
	}

	private OnItemClickListener listViewOnClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			// Ultils.sendTrackToPlayer(getActivity(), track_list, list);
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList(constants.TRACKS_KEY, track_list);
			bundle.putInt(constants.TRACK_INDEX_KEY, arg2);
			Intent intent = new Intent(getActivity(), PlayerActivity.class);
			intent.putExtras(bundle);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
	};

	@Override
	public void onScroll(AbsListView view, int offsetVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		int lastInScreen = offsetVisibleItem + visibleItemCount;
		if ((lastInScreen == totalItemCount) && !loadingMore) {
			offset += COUNT_ITEM_LOAD_MORE;
			tmpQuery += "&offset=" + offset + "&limit=" + COUNT_ITEM_LOAD_MORE;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new LoadMoreDataTask()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				new LoadMoreDataTask().execute();
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}
}
