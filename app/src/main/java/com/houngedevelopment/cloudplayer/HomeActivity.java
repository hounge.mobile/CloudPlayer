package com.houngedevelopment.cloudplayer;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.adapter.DrawerMenuAdapter;
import com.bk.lrandom.yourcloudplayer.business.ScandDownloadedTrack;
import com.bk.lrandom.yourcloudplayer.business.Ultils;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.fragments.AboutUsFragment;
import com.bk.lrandom.yourcloudplayer.fragments.DownloadedFragment;
import com.bk.lrandom.yourcloudplayer.fragments.ExploreStreamFragment;
import com.bk.lrandom.yourcloudplayer.fragments.FanpageFragment;
import com.bk.lrandom.yourcloudplayer.fragments.GendersFragment;
import com.bk.lrandom.yourcloudplayer.models.DrawerMenuItem;
import com.bk.lrandom.yourcloudplayer.models.Track;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class HomeActivity extends ActionBarParentActivity {
	DrawerLayout drawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	ListView lst;
	Intent intent;
	ArrayList<DrawerMenuItem> drawerItems;
	DrawerMenuAdapter drawerMenuAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_layout);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		lst = (ListView) findViewById(R.id.list_slidermenu);

		ArrayAdapter<String> drawerMenuTitles = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, getResources()
						.getStringArray(R.array.drawer_menus));

		drawerItems = new ArrayList<DrawerMenuItem>();
		drawerItems.add(new DrawerMenuItem(drawerMenuTitles.getItem(0),
				R.drawable.ic_navigate));
		drawerItems.add(new DrawerMenuItem(drawerMenuTitles.getItem(1),
				R.drawable.ic_gender));
		drawerItems.add(new DrawerMenuItem(drawerMenuTitles.getItem(3),
				R.drawable.ic_downloaded));
		drawerItems.add(new DrawerMenuItem(drawerMenuTitles.getItem(4),
				R.drawable.ic_offline_music));
		drawerItems.add(new DrawerMenuItem(drawerMenuTitles.getItem(6),
				R.drawable.ic_fanpage));
		drawerItems.add(new DrawerMenuItem(drawerMenuTitles.getItem(7),
				R.drawable.ic_info));
		drawerMenuAdapter = new DrawerMenuAdapter(this, drawerItems);

		lst.setAdapter(drawerMenuAdapter);
		lst.setOnItemClickListener(new DrawMenuClickListener());
		mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.drawable.ic_navigation_drawer, R.string.app_name,
				R.string.app_name) {
			@Override
			public void onDrawerClosed(View drawerView) {
				// TODO Auto-generated method stub
				supportInvalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				// TODO Auto-generated method stub
				super.onDrawerOpened(drawerView);
				supportInvalidateOptionsMenu();
			}
		};
		drawerLayout.setDrawerListener(mDrawerToggle);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		loadView(0);
		Ultils.doFirstRun(this, MODE_PRIVATE);
	}

	private class DrawMenuClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			// TODO Auto-generated method stub
			loadView(position);
		}
	}

	private void loadView(int position) {
		Fragment fragment = null;
		switch (position) {
		case 0:
			if (!Ultils.isConnectingToInternet(HomeActivity.this)) {
				showMsg(getResources().getString(R.string.open_network));
			}
			fragment = ExploreStreamFragment.newInstance();
			changeActionBarTitle(getResources().getString(
					R.string.explore_stream));
			break;

		case 1:
			fragment = GendersFragment.newInstance();
			changeActionBarTitle(getResources().getString(R.string.genders));
			break;

		case 2:
			ScandDownloadedTrack trackDownloadedScanner = new ScandDownloadedTrack(
					getResources().getString(R.string.download_folder));
			ArrayList<Track> tracksDownloaded = trackDownloadedScanner
					.getDownloadedTrack();
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList(constants.TRACKS_KEY,
					tracksDownloaded);
			fragment = DownloadedFragment.newInstance();
			changeActionBarTitle(getResources().getString(
					R.string.downloaded_label));
			fragment.setArguments(bundle);
			break;

		case 3:
			Intent intent = new Intent(HomeActivity.this,
					ExplorationActivity.class);
			startActivity(intent);
			break;
		case 4:
			if (!Ultils.isConnectingToInternet(HomeActivity.this)) {
				showMsg(getResources().getString(R.string.open_network));
			}
			fragment = FanpageFragment.newInstance();
			changeActionBarTitle(getResources().getString(R.string.fanpage));
			break;

		case 5:
			fragment = AboutUsFragment.newInstance();
			changeActionBarTitle(getResources().getString(R.string.about_us));
			break;
		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.content, fragment)
					.commit();
			lst.setItemChecked(position, true);
			lst.setSelection(position);
			drawerLayout.closeDrawer(lst);
		} else {
			Log.e("HomeActivity", "Error creating fragment");
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {

		default:
			break;
		}

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
}
