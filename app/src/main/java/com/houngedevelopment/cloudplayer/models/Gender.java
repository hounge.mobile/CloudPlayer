package com.houngedevelopment.cloudplayer.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Gender implements Parcelable {
	public static final String TAG_TITLE = "title";
	public static final String TAG_NAME = "name";
	String name;
    String title;
	
	public Gender() {
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Gender(Parcel in) {
		title=in.readString();
		name = in.readString();
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(title);
		dest.writeString(name);
	}

	public static final Creator<Gender> CREATOR = new Creator<Gender>() {
		@Override
		public Gender createFromParcel(Parcel in) {
			// TODO Auto-generated method stub
			return new Gender(in);
		}

		@Override
		public Gender[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Gender[size];
		}
	};
}
