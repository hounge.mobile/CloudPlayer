package com.houngedevelopment.cloudplayer.models;

import android.R.bool;
import android.os.Parcel;
import android.os.Parcelable;

public class Track implements Parcelable {
	// JSON TAG FOR REMOTE TRACK
	public static final String TAG_ID = "id";
	public static final String TAG_TITLE = "title";
	public static final String TAG_ARTIST = "username";
	public static final String TAG_COUNT_LIKES = "likes";
	public static final String TAG_PATH = "mp3";
	public static final String TAG_LYRICS = "lyric";
	public static final String TAG_USER = "user";
	public static final String TAG_THUMB = "artwork_url";
	public static final String TAG_SC_PATH = "stream_url";
	public static final String TAG_DOWNLOADABLE = "downloadable";
	public static final String TAG_DOWNLOAD = "download_url";
	public static final String TAG_FILE_FORMAT ="original_format";

	private int id;
	private int remote_id;
	int countLike;
	private String title;
	private String artist;
	private String path;
	private String size;
	private String duration;
	private String extension;
	private int albumId;
	private String lyric;
	private String thumb;
	private String download_url;
	private String downloadable;


	public String getDownload_url() {
		return download_url;
	}

	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}

	public String getDownloadable() {
		return downloadable;
	}

	public void setDownloadable(String downloadable) {
		this.downloadable = downloadable;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getLyric() {
		return lyric;
	}

	public void setLyric(String lyric) {
		this.lyric = lyric;
	}

	private int downloadFlag = 0;

	public int getDownloadFlag() {
		return downloadFlag;
	}

	public void setDownloadFlag(int downloadFlag) {
		this.downloadFlag = downloadFlag;
	}

	public int getRemote_id() {
		return remote_id;
	}

	public void setRemote_id(int remote_id) {
		this.remote_id = remote_id;
	}

	public int getCountLike() {
		return countLike;
	}

	public void setCountLike(int countLike) {
		this.countLike = countLike;
	}

	private Boolean selected = false;
	private Boolean checked = false;

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Track() {

	}

	public Track(Parcel in) {
		id = in.readInt();
		title = in.readString();
		artist = in.readString();
		path = in.readString();
		size = in.readString();
		duration = in.readString();
		extension = in.readString();
		albumId = in.readInt();
		remote_id = in.readInt();
		countLike = in.readInt();
		downloadFlag = in.readInt();
		lyric = in.readString();
		remote_id = in.readInt();
		thumb = in.readString();
		downloadable=in.readString();
		download_url=in.readString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(title);
		dest.writeString(artist);
		dest.writeString(path);
		dest.writeString(size);
		dest.writeString(duration);
		dest.writeString(extension);
		dest.writeInt(albumId);
		dest.writeInt(countLike);
		dest.writeInt(remote_id);
		dest.writeInt(downloadFlag);
		dest.writeString(lyric);
		dest.writeInt(remote_id);
		dest.writeString(thumb);
		dest.writeString(downloadable);
		dest.writeString(download_url);
	}

	public static final Creator<Track> CREATOR = new Creator<Track>() {

		public Track createFromParcel(Parcel in) {
			// TODO Auto-generated method stub
			return new Track(in);
		}

		public Track[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Track[size];
		}

	};
}
