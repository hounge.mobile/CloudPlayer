package com.houngedevelopment.cloudplayer.interfaces;

public interface CustomConfirmDialogFragmentComunicator {
   void positiveCallback();
   void negativeCallback();
}
