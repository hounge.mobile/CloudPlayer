package com.houngedevelopment.cloudplayer.interfaces;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.models.Track;

public interface TrackInQueueFragmentComunicator {
	void LoadTrackAndPlay(int trackIndex, ArrayList<Track> tracks,
						  Boolean setPager);
}
