package com.houngedevelopment.cloudplayer.interfaces;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.models.Track;

public interface CustomSelectTrackDialogComunicator {
  void savePlaylist(ArrayList<Track> tracks);
}
