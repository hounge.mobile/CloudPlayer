package com.houngedevelopment.cloudplayer.business;

import java.io.File;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.PlayerActivity;
import com.bk.lrandom.yourcloudplayer.adapter.TrackAdapter;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.dals.TrackDal;
import com.bk.lrandom.yourcloudplayer.models.Track;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import android.R.bool;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class Ultils {
	public static String Timer(long milliseconds) {
		String finalTimer = "";
		String secondsString = "";
		int hours = (int) (milliseconds / (1000 * 60 * 60));
		int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
		int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
		if (hours > 0) {
			finalTimer = hours + ":";
		}
		if (seconds < 10) {
			secondsString = "0" + seconds;
		} else {
			secondsString = "" + seconds;
		}
		finalTimer = finalTimer + minutes + ":" + secondsString;
		return finalTimer;
	}

	public static int getProgressPercentage(long currentDuration,
			long totalDuration) {
		Double percentage = (double) 0;
		long currentSeconds = (int) (currentDuration / 1000);
		long totalSeconds = (int) (totalDuration / 1000);
		percentage = (((double) currentSeconds) / totalSeconds) * 100;
		return percentage.intValue();
	}

	public static int progressToTimer(int progress, int totalDuration) {
		int currentDuration = 0;
		totalDuration = (int) (totalDuration / 1000);
		currentDuration = (int) ((((double) progress) / 100) * totalDuration);
		return currentDuration * 1000;
	}

	public static void setRingtone(int pos, Track track, Context context) {
		File file = new File(track.getPath());
		if (file.exists()) {
			ContentValues values = new ContentValues();
			values.put(MediaStore.MediaColumns._ID, track.getId());
			values.put(MediaStore.MediaColumns.DATA, track.getPath());
			values.put(MediaStore.MediaColumns.TITLE, track.getTitle());
			values.put(MediaStore.MediaColumns.SIZE, track.getSize());
			values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/*");
			values.put(MediaStore.Audio.Media.ARTIST, track.getArtist());
			values.put(MediaStore.Audio.Media.DURATION, track.getDuration());
			values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
			values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
			values.put(MediaStore.Audio.Media.IS_ALARM, false);
			values.put(MediaStore.Audio.Media.IS_MUSIC, false);
			Uri uri = MediaStore.Audio.Media.getContentUriForPath(track
					.getPath());
			TrackDal audioDAL = new TrackDal(context);
			audioDAL.removeTracksOnMDS(uri, MediaStore.MediaColumns.DATA + "='"
					+ track.getPath() + "'");
			Uri newUri = audioDAL.insertTracksOnMDS(uri, values);
			RingtoneManager.setActualDefaultRingtoneUri(context,
					RingtoneManager.TYPE_RINGTONE, newUri);
			Toast ts = Toast
					.makeText(
							context,
							track.getTitle()
									+ " "
									+ context
											.getResources()
											.getString(
													R.string.track_has_been_set_as_ringtone_msg),
							Integer.parseInt(context.getResources().getString(
									R.string.toast_time_out)));
			ts.show();
		} else {
			Toast ts = Toast.makeText(context, context.getResources()
					.getString(R.string.track_cannot_been_set_as_ringtone_msg),
					Integer.parseInt(context.getResources().getString(
							R.string.toast_time_out)));
			ts.show();
		}
	}

	public static Boolean removeTrack(final Context context, final Track track) {
		final ContentResolver contentResolver = context.getContentResolver();
		File file = new File(track.getPath());
		Uri uri = MediaStore.Audio.Media.getContentUriForPath(track.getPath());
		contentResolver.delete(uri,
				MediaStore.Audio.Media._ID + "='" + track.getId() + "'", null);
		TrackDal trackDAL = new TrackDal(context);
		trackDAL.getConnect();
		trackDAL.removeTracksByID(track.getId());
		trackDAL.close();
		boolean deleted = file.delete();
		return deleted;
	}

	public static void sendTrackToPlayer(final Context context,
			final ArrayList<Track> tracks, ListView list) {
		if (!tracks.isEmpty()) {
			TrackAdapter trackAdapter = new TrackAdapter(context,
					R.layout.track_item_layout, tracks);
			list.setAdapter(trackAdapter);
			list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@SuppressLint("InlinedApi")
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					Bundle bundle = new Bundle();
					bundle.putParcelableArrayList(constants.TRACKS_KEY, tracks);
					bundle.putInt(constants.TRACK_INDEX_KEY, arg2);
					Intent intent = new Intent(context, PlayerActivity.class);
					intent.putExtras(bundle);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					context.getApplicationContext().startActivity(intent);
				}
			});
		}
	}

	public static String md5(String input) {
		String md5 = null;
		if (null == input)
			return null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return md5;
	}

	public static boolean createDirOnSDCard(String dirName) {
		File folder = new File(Environment.getExternalStorageDirectory() + "/"
				+ dirName);
		boolean success = true;
		if (!folder.exists()) {
			success = folder.mkdir();
		}
		return success;
	}

	public static boolean remoteFileExists(String URLName) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(URLName)
					.openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean isConnectingToInternet(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}

	public static String getFileName(URL extUrl) {
		String filename = "";
		String path = extUrl.getPath();
		String[] pathContents = path.split("[\\\\/]");
		if (pathContents != null) {
			int pathContentsLength = pathContents.length;
			System.out.println("Path Contents Length: " + pathContentsLength);
			for (int i = 0; i < pathContents.length; i++) {
				System.out.println("Path " + i + ": " + pathContents[i]);
			}
			String lastPart = pathContents[pathContentsLength - 1];
			String[] lastPartContents = lastPart.split("\\.");
			if (lastPartContents != null && lastPartContents.length > 1) {
				int lastPartContentLength = lastPartContents.length;
				System.out
						.println("Last Part Length: " + lastPartContentLength);
				String name = "";
				for (int i = 0; i < lastPartContentLength; i++) {
					System.out.println("Last Part " + i + ": "
							+ lastPartContents[i]);
					if (i < (lastPartContents.length - 1)) {
						name += lastPartContents[i];
						if (i < (lastPartContentLength - 2)) {
							name += ".";
						}
					}
				}
				String extension = lastPartContents[lastPartContentLength - 1];
				filename = name + "." + extension;
				// System.out.println("Name: " + name);
				// System.out.println("Extension: " + extension);
				// System.out.println("Filename: " + filename);
			}
		}
		return filename;
	}

	public static final void doFirstRun(Context context, int MODE) {
		SharedPreferences settings = context.getSharedPreferences("first_run",
				MODE);
		if (settings.getBoolean("isFirstRun", true)) {
			Builder builder = new Builder(context);
			builder.setMessage(context.getResources().getString(
					R.string.long_term_content));
			builder.setTitle(context.getResources().getString(
					R.string.long_term_label));
			builder.setPositiveButton(
					context.getResources().getString(R.string.ok_label),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean("isFirstRun", false);
			editor.commit();
		}
	}
}
