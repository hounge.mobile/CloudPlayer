package com.houngedevelopment.cloudplayer.business;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.bk.lrandom.yourcloudplayer.models.Gender;

public class XMLGenderParser {
	public static ArrayList<Gender> parse(InputStream is) {
		ArrayList<Gender> channels = null;
		try {
			XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser()
					.getXMLReader();
			XMLGendersHandler xmlGenderHandler = new XMLGendersHandler();
			xmlReader.setContentHandler(xmlGenderHandler);
			xmlReader.parse(new InputSource(is));
			channels = xmlGenderHandler.getGenders();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return channels;
	}
}