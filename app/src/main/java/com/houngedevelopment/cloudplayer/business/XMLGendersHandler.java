package com.houngedevelopment.cloudplayer.business;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.bk.lrandom.yourcloudplayer.models.Gender;

public class XMLGendersHandler extends DefaultHandler {
	public static final String GENDER_TAG = "gender";
	public static final String TITLE_TAG = "title";
	public static final String NAME_ATTR = "name";

	private ArrayList<Gender> genders;
	private Gender gender;
	private StringBuilder tmp;

	public XMLGendersHandler() {
		// TODO Auto-generated constructor stub
		genders = new ArrayList<Gender>();
	}

	public ArrayList<Gender> getGenders() {
		return genders;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		tmp = new StringBuilder();
		if (qName.equalsIgnoreCase(GENDER_TAG)) {
			gender = new Gender();
			gender.setName(attributes.getValue(NAME_ATTR));
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		tmp.append(new String(ch, start, length));
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		if (gender != null) {
			if (qName.equalsIgnoreCase(GENDER_TAG)) {
				genders.add(gender);
			} else if (qName.equalsIgnoreCase(TITLE_TAG)) {
				gender.setTitle(tmp.toString().trim());
				Log.i("CHANE", tmp.toString());
			}
		}
	}
}
