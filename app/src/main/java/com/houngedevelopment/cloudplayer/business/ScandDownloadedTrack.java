package com.houngedevelopment.cloudplayer.business;

import java.io.File;
import java.util.ArrayList;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.ID3v1Tag;

import com.bk.lrandom.yourcloudplayer.models.Track;

import android.os.Environment;
import android.util.Log;

public class ScandDownloadedTrack {
	String dir;
	private ArrayList<Track> tracks;
	private String mp3Pattern = ".mp3";
	private String mediaPath = null;

	public ScandDownloadedTrack() {
		// TODO Auto-generated constructor stub
	}

	public ScandDownloadedTrack(String dir) {
		tracks = new ArrayList<Track>();
		this.dir = dir;
		this.mediaPath = Environment.getExternalStorageDirectory() + "/" + dir;
	}

	public ArrayList<Track> getDownloadedTrack() {
		if (mediaPath != null) {
			File home = new File(mediaPath);
			File[] listFiles = home.listFiles();
			if (listFiles != null && listFiles.length > 0) {
				for (File file : listFiles) {
					System.out.println(file.getAbsolutePath());
					if (file.isDirectory()) {
						scanDirectory(file);
					} else {
						addSongToList(file);
					}
				}
			}
		}
		return tracks;
	}

	private void scanDirectory(File directory) {
		if (directory != null) {
			File[] listFiles = directory.listFiles();
			if (listFiles != null && listFiles.length > 0) {
				for (File file : listFiles) {
					if (file.isDirectory()) {
						scanDirectory(file);
					} else {
						addSongToList(file);
					}
				}
			}
		}
	}

	private void addSongToList(File song) {
		if (song.getName().endsWith(mp3Pattern)) {
			Track track = new Track();
			MP3File f;
			try {
				f = (MP3File) AudioFileIO.read(song);
				Tag tag = f.getTag();
				if (!f.hasID3v1Tag()) {
					track.setTitle(song.getName());
					track.setArtist("<unknown>");
				} else {
					track.setTitle(tag.getFirst(FieldKey.TITLE));
					Log.i("TAG_TITLE", tag.getFirst(FieldKey.TITLE));
					track.setArtist(tag.getFirst(FieldKey.ARTIST));
				}
				track.setPath(song.getAbsolutePath());
				tracks.add(track);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
