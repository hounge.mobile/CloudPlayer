package com.houngedevelopment.cloudplayer.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.UUID;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.DownloadProgressActivity;
import com.bk.lrandom.yourcloudplayer.business.Ultils;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.dals.HistoryDal;
import com.bk.lrandom.yourcloudplayer.models.Track;

import android.R.integer;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.CalendarContract.Reminders;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

public class AudioDownloaderService extends IntentService {
	ArrayList<Track> tracks;
	Track track;
	InputStream input;
	public static final int NOTIFICATION_ID = 150;
	public static final String DOWNLOAD_STATE = "ERROR_DOWNLOAD";
	public static final int OK_CODE = 0;
	public static final int NO_NETWORK_CODE = 1;
	public static final int FILE_NOT_FOUND_CODE = 2;
	public static final int CANCEL_DOWNLOAD_CODE = 3;
	public static final String BROADCAST_ACTION = "com.bk.lrandom.multilpodcast.audiodownloaderService";
	Intent intent;
	private ArrayList<Integer> stopList = new ArrayList<Integer>();
	private ArrayList<Track> inQueueDownload = new ArrayList<Track>();
	public static final String PERCENT_KEY = "PERCENT_KEY";
	public static final String STOP_TRACK_KEY = "STOP_TRACK";
	public static final String SEND_TRACK_KEY = "SEND_TRACK_KEY";
	public static final String GET_TRACKS_KEY = "GET_TRACKS_KEY";
	public static final String CURRENT_ID_KEY = "CURRENT_ID_KEY";
	NotificationManager notifyManager;
	NotificationCompat.Builder builder;
	OutputStream output;
	String filePath;
	int downloadedPercent;
	Track globalTrack;

	public AudioDownloaderService() {
		// TODO Auto-generated constructor stub
		super("");
	}

	public AudioDownloaderService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		intent = new Intent(BROADCAST_ACTION);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		if (intent.hasExtra(STOP_TRACK_KEY)) {
			int index = intent.getExtras().getInt(
					AudioDownloaderService.STOP_TRACK_KEY);
			stopList.add(inQueueDownload.get(index).getRemote_id());
			inQueueDownload.remove(index);
			// sendUpdateUI();
		}

		if (intent.hasExtra(SEND_TRACK_KEY)) {
			track = intent.getParcelableExtra(SEND_TRACK_KEY);
			Log.i("ADD TRACK TO QUEUE", track.getRemote_id() + "");
			inQueueDownload.add(track);
		}

		if (intent.hasExtra(GET_TRACKS_KEY)) {
			sendUpdateUI();
		}
		return super.onStartCommand(intent, flags, startId);
	}

	private void sendUpdateUI() {
		intent = new Intent(BROADCAST_ACTION);
		intent.putExtra(constants.TRACKS_KEY, inQueueDownload);
		sendBroadcast(intent);
	}

	private int findItemById(ArrayList<Integer> stopLists, int remote_id) {
		for (int i = 0; i < stopLists.size(); i++) {
			if (stopList.get(i) == remote_id) {
				return i;
			}
		}
		return -1;
	}

	private void downloadFile(Track track) {
		if (Ultils.isConnectingToInternet(this)) {
			globalTrack = track;
			int count;
			int timer = 0;
			notifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			builder = new NotificationCompat.Builder(this);
			intent = new Intent(this, DownloadProgressActivity.class);
			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					intent, 0);
			builder.setContentTitle(globalTrack.getTitle())
					.setContentText(
							this.getResources().getString(
									R.string.download_in_progress))
					.setSmallIcon(R.drawable.ic_download)
					.setContentIntent(contentIntent);
			builder.setProgress(0, 0, true);
			notifyManager.notify(NOTIFICATION_ID, builder.build());
			try {
				URL url = new URL(globalTrack.getPath());
				URLConnection conection = null;
				try {
					conection = url.openConnection();
				} catch (Exception e) {
					// TODO: handle exception
					Log.i("CANOT OPEN CONNEC", "CANNOT OPEN CONNECTION");
				}
				try {
					conection.connect();
				} catch (Exception e) {
					// TODO: handle exception
					Log.i("CANOT CONNECT", "CANNOT CONNECT");
				}
				;
				int lenghtOfFile = conection.getContentLength();
				try {
					boolean success = Ultils.createDirOnSDCard(getResources()
							.getString(R.string.download_folder));
					if (success) {
						input = new BufferedInputStream(url.openStream(), 8192);
						filePath = Environment.getExternalStorageDirectory()
								+ "/"
								+ getResources().getString(
										R.string.download_folder) + "/"
								+ globalTrack.getTitle()
								+ globalTrack.getRemote_id() + "."
								+ "mp3";
						output = new FileOutputStream(filePath);
						byte data[] = new byte[1024];
						long total = 0;
						while ((count = input.read(data)) != -1) {
							int index = findItemById(stopList,
									globalTrack.getRemote_id());
							timer++;
							if (index >= 0) {
								File file = new File(filePath);
								file.delete();
								builder.setContentText(getResources()
										.getString(R.string.download_cancel));
								notifyManager.cancel(NOTIFICATION_ID);
								output.flush();
								output.close();
								input.close();
								Log.i("CANCEL DOWNLOAD", "CANCEL DOWNLOAD");
								inQueueDownload.remove(globalTrack);
								sendUpdateUI();
								intent = new Intent(BROADCAST_ACTION);
								intent.putExtra(DOWNLOAD_STATE,
										CANCEL_DOWNLOAD_CODE);
								intent.putExtra(CURRENT_ID_KEY, globalTrack);
								intent.putExtra(PERCENT_KEY, 0);
								sendBroadcast(intent);
								updateDownloadState(HistoryDal.PENDDING_FLAG,
										globalTrack);
								return;
							}
							total += count;
							downloadedPercent = (int) (total * 100)
									/ lenghtOfFile;
							output.write(data, 0, count);
							if (timer >= 1000) {
								timer = 0;
								builder.setContentText(downloadedPercent + " %");
								builder.setProgress(100, downloadedPercent,
										false);
								notifyManager.notify(NOTIFICATION_ID,
										builder.build());
								intent = new Intent(BROADCAST_ACTION);
								intent.putExtra(CURRENT_ID_KEY, globalTrack);
								intent.putExtra(PERCENT_KEY, downloadedPercent);
								sendBroadcast(intent);
							}
						}// end while
						builder.setContentText(getResources().getString(
								R.string.download_ok));
						notifyManager.cancel(NOTIFICATION_ID);
						output.flush();
						output.close();
						input.close();
						inQueueDownload.remove(globalTrack);
						sendUpdateUI();
						intent = new Intent(BROADCAST_ACTION);
						intent.putExtra(DOWNLOAD_STATE, OK_CODE);
						intent.putExtra(CURRENT_ID_KEY, globalTrack);
						updateDownloadState(HistoryDal.DOWNLOADED_FLAG,
								globalTrack);
						sendBroadcast(intent);
						// download done
					}
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("ERROR DOWNLOAD", "error download");
					notifyManager.cancel(NOTIFICATION_ID);
					intent = new Intent(BROADCAST_ACTION);
					intent.putExtra(CURRENT_ID_KEY, globalTrack);
					intent.putExtra(DOWNLOAD_STATE, NO_NETWORK_CODE);
					inQueueDownload.remove(globalTrack);
					sendBroadcast(intent);
					updateDownloadState(HistoryDal.PENDDING_FLAG, globalTrack);
					sendUpdateUI();
					try {
						File file = new File(filePath);
						file.delete();
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
			} catch (Exception e) {
				notifyManager.cancel(NOTIFICATION_ID);
				intent = new Intent(BROADCAST_ACTION);
				intent.putExtra(CURRENT_ID_KEY, globalTrack.getRemote_id());
				intent.putExtra(DOWNLOAD_STATE, NO_NETWORK_CODE);
				updateDownloadState(HistoryDal.PENDDING_FLAG, globalTrack);
				inQueueDownload.remove(globalTrack);
				sendBroadcast(intent);
				sendUpdateUI();
				try {
					File file = new File(filePath);
					file.delete();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		} else {
			try {
				File file = new File(filePath);
				file.delete();
			} catch (Exception e2) {
				// TODO: handle exception
			}
			notifyManager.cancel(NOTIFICATION_ID);
			intent = new Intent(BROADCAST_ACTION);
			intent.putExtra(CURRENT_ID_KEY, globalTrack.getRemote_id());
			intent.putExtra(DOWNLOAD_STATE, NO_NETWORK_CODE);
			inQueueDownload.remove(globalTrack);
			updateDownloadState(HistoryDal.PENDDING_FLAG, globalTrack);
			sendBroadcast(intent);
			sendUpdateUI();
		}// end if
	}// end,

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Log.i("ON HANDLER - DOWNLOADER - SERVICES", "handler service");
		if (intent.hasExtra(SEND_TRACK_KEY)) {
			track = intent.getParcelableExtra(SEND_TRACK_KEY);
			int index = findItemById(stopList, track.getRemote_id());
			if (index < 0) {
				downloadFile(track);
			}
		}
	}

	private void updateDownloadState(int flag, Track track) {
		HistoryDal historyDal = new HistoryDal(AudioDownloaderService.this);
		historyDal.getConnect();
		historyDal.insertTrack(track, flag);
		historyDal.close();
	}
}
