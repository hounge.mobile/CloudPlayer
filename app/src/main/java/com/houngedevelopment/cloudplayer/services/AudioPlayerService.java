package com.houngedevelopment.cloudplayer.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.PlayerActivity;
import com.bk.lrandom.yourcloudplayer.business.Ultils;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.models.Track;
import android.R.bool;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.support.v4.app.NotificationCompat;

public class AudioPlayerService extends Service implements
		OnCompletionListener, OnBufferingUpdateListener, OnPreparedListener,
		OnErrorListener {
	MediaPlayer mediaPlayer;
	NotificationManager notificationManager;
	boolean isRepeat, isShuffle, isBuffering = false,
			isPlayAfterBuffering = true;
	int trackIndex;
	ArrayList<Track> tracks;
	private final IBinder mBinder = new PlayerBinder();
	private int NOTIFICATION_ID = 111;
	public static final String ON_COMPLETE_BROADCAST_ACTION = "com.bk.lrandom.yourcloudplayer.audioplayerService";
	Intent intent;
	private int buffer;
	BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
			if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
				// Call Dropped or rejected � Restart service play media
				resume();
			} else {
				pause();
			}
		}
	};

	public class PlayerBinder extends Binder {
		public AudioPlayerService getService() {
			return AudioPlayerService.this;
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	public void onCreate() {
		super.onCreate();
		// instance player object
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnCompletionListener(this);
		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnErrorListener(this);
		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		intent = new Intent(ON_COMPLETE_BROADCAST_ACTION);
		IntentFilter filter = new IntentFilter();
		filter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
		registerReceiver(receiver, filter);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		try {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
			mediaPlayer.release();
		} catch (Exception e) {
			// TODO: handle exception
		}
		Log.i("DESTROY SERVICE", "destroy");
		unregisterReceiver(receiver);
	}

	public void play(int trackIndex, ArrayList<Track> tracks) {
		try {
			if (mediaPlayer != null) {
				if (tracks.get(trackIndex).getRemote_id() != 0) {
					if (!Ultils.isConnectingToInternet(AudioPlayerService.this)) {
						return;
					}
				}
				this.tracks = tracks;
				this.trackIndex = trackIndex;
				this.buffer = 0;
				mediaPlayer.reset();
				mediaPlayer.setDataSource(tracks.get(trackIndex).getPath()
						.replace("https://", "http://"));
				mediaPlayer.setOnPreparedListener(this);
				mediaPlayer.prepareAsync();
				isBuffering = true;
				isPlayAfterBuffering = true;
				showNotification();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Log.i("LOAD FILE", "ERROR");
		}
	};

	public void pause() {
		try {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				cancelNotification();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void resume() {
		try {
			if (mediaPlayer != null) {
				mediaPlayer.start();
				showNotification();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public int getTotalTime() {
		if (isBuffering == false) {
			return mediaPlayer.getDuration();
		}
		return 0;
	}

	public int getElapsedTime() {
		return mediaPlayer.getCurrentPosition();
	}

	public boolean getBuffering() {
		return this.isBuffering;
	}

	public void setBuffering(boolean flag) {
		this.isBuffering = flag;
	}

	public boolean getPlayAfterBuffering() {
		return this.isPlayAfterBuffering;
	}

	public void setPlayAfterBuffering(boolean flag) {
		if (flag == false) {
			cancelNotification();
		} else {
			showNotification();
		}
		this.isPlayAfterBuffering = flag;
	}

	public void seek(int pos) {
		mediaPlayer.seekTo(pos);
	}

	public boolean isPlay() {
		if (mediaPlayer.isPlaying()) {
			return true;
		} else {
			return false;
		}
	}

	public void setRepeat(boolean flag) {
		this.isRepeat = flag;
	}

	public void setShuffle(boolean flag) {
		this.isShuffle = flag;
	}

	public boolean getRepeat() {
		return this.isRepeat;
	}

	public boolean getShuffle() {
		return this.isShuffle;
	}

	public ArrayList<Track> getTrack() {
		return this.tracks;
	}

	public int getTrackIndex() {
		return this.trackIndex;
	}

	public void showNotification() {
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
				getApplicationContext());

		Intent intent = new Intent(this, PlayerActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				intent, 0);
		notificationBuilder.setContentTitle(tracks.get(trackIndex).getTitle())
				.setContentText(tracks.get(trackIndex).getArtist())
				.setSmallIcon(R.drawable.ic_small_logo)
				.setContentIntent(contentIntent);
		Notification notification = notificationBuilder.build();
		notificationManager.notify(NOTIFICATION_ID, notification);
		startForeground(NOTIFICATION_ID, notification);
	}

	public void cancelNotification() {
		stopForeground(true);
	}

	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		try {
			if (!isBuffering) {
				tracks.get(trackIndex).setSelected(false);
				if (!this.isRepeat) {
					if (!this.isShuffle) {
						if (this.trackIndex == (this.tracks.size() - 1)) {
							this.trackIndex = 0;
						} else {
							this.trackIndex += 1;
						}
					} else {
						Random rand = new Random();
						this.trackIndex = rand
								.nextInt((this.tracks.size() - 1) - 0 + 1) + 0;
					}
				}
				tracks.get(trackIndex).setSelected(true);
				if (this.isRepeat) {
					mp.seekTo(0);
					mp.start();
				} else {
					play(trackIndex, tracks);
				}
				intent.putExtra(constants.TRACK_INDEX_KEY, trackIndex + "");
				sendBroadcast(intent);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		if (isPlayAfterBuffering) {
			try {
				mediaPlayer.start();
			} catch (Exception e) {
				Log.i("cannot", "fuck");
				// TODO: handle exception
			}
		}
		isBuffering = false;
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		// TODO Auto-generated method stub
		this.buffer = percent;
	}

	public int getBufferingDownload() {
		return this.buffer;
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		if (tracks != null && trackIndex == tracks.size()) {
			trackIndex = 0;
		} else {
			trackIndex++;
		}
		play(trackIndex, tracks);
		intent.putExtra(constants.TRACK_INDEX_KEY, trackIndex + "");
		sendBroadcast(intent);
		return true;
	}
}
