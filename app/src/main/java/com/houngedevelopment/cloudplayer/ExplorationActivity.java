package com.houngedevelopment.cloudplayer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import com.bk.lrandom.yourcloudplayer.R;

import com.bk.lrandom.yourcloudplayer.adapter.TabsPagerAdapter;
import com.bk.lrandom.yourcloudplayer.business.ScandDownloadedTrack;
import com.bk.lrandom.yourcloudplayer.business.Ultils;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.dals.AlbumDal;
import com.bk.lrandom.yourcloudplayer.dals.ArtistDal;
import com.bk.lrandom.yourcloudplayer.dals.HistoryDal;
import com.bk.lrandom.yourcloudplayer.dals.PlaylistDal;
import com.bk.lrandom.yourcloudplayer.dals.TrackDal;
import com.bk.lrandom.yourcloudplayer.factory.ExplorationTabFactory;
import com.bk.lrandom.yourcloudplayer.fragments.AlbumFragment;
import com.bk.lrandom.yourcloudplayer.fragments.ArtistFragment;
import com.bk.lrandom.yourcloudplayer.fragments.CustomSelectTrackDialogFragment;
import com.bk.lrandom.yourcloudplayer.fragments.DownloadedFragment;
import com.bk.lrandom.yourcloudplayer.fragments.HistoryFragment;
import com.bk.lrandom.yourcloudplayer.fragments.PlaylistFragment;
import com.bk.lrandom.yourcloudplayer.fragments.TrackFragment;
import com.bk.lrandom.yourcloudplayer.interfaces.CustomPromptDialogFragmentComunicator;
import com.bk.lrandom.yourcloudplayer.interfaces.CustomSelectPlaylistDialogComunicator;
import com.bk.lrandom.yourcloudplayer.interfaces.CustomSelectTrackDialogComunicator;
import com.bk.lrandom.yourcloudplayer.models.Album;
import com.bk.lrandom.yourcloudplayer.models.Artist;
import com.bk.lrandom.yourcloudplayer.models.Playlist;
import com.bk.lrandom.yourcloudplayer.models.Track;
import com.bk.lrandom.yourcloudplayer.services.AudioDownloaderService;
import com.bk.lrandom.yourcloudplayer.services.AudioPlayerService;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.internal.widget.ScrollingTabContainerView.TabView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

public class ExplorationActivity extends ActionBarParentActivity implements
		TabListener, CustomPromptDialogFragmentComunicator,
		CustomSelectTrackDialogComunicator,
		CustomSelectPlaylistDialogComunicator {
	private ViewPager viewPager;
	private TabsPagerAdapter tabPagerAdapter;
	private ArrayList<Fragment> fragments;
	private ArrayList<String> titles;
	LinkedHashSet<Integer> disabledItems = new LinkedHashSet<Integer>();
	LinkedHashSet<Integer> enableItems = new LinkedHashSet<Integer>();
	TrackDal trackDal;
	String playlistName;
	Receiver receiver = new Receiver();

	private void setTabFragments() {
		fragments = new ArrayList<Fragment>();
		titles = new ArrayList<String>();

		ScandDownloadedTrack trackDownloadedScanner = new ScandDownloadedTrack(
				getResources().getString(R.string.download_folder));
		ArrayList<Track> tracksDownloaded = trackDownloadedScanner
				.getDownloadedTrack();
		Bundle bundle = new Bundle();
		bundle.putParcelableArrayList(constants.TRACKS_KEY, tracksDownloaded);
		DownloadedFragment downloadedFragment = DownloadedFragment
				.newInstance();
		downloadedFragment.setArguments(bundle);

		TrackDal trackDal = new TrackDal(this);
		ArrayList<Track> tracks = trackDal.getTracksOnMDS();
		bundle = new Bundle();
		bundle.putParcelableArrayList(constants.TRACKS_KEY, tracks);
		TrackFragment trackFragment = TrackFragment.newInstance();
		trackFragment.setArguments(bundle);
		trackDal.close();

		ArtistFragment artistFragment = ArtistFragment.newInstance();
		ArtistDal artistDal = new ArtistDal(this);
		ArrayList<Artist> artists = artistDal.getArtistsOnMDS();
		bundle = new Bundle();
		bundle.putParcelableArrayList(constants.ARTISTS_KEY, artists);
		artistFragment.setArguments(bundle);
		artistDal.close();

		AlbumFragment albumFragment = AlbumFragment.newInstance();
		AlbumDal albumDal = new AlbumDal(this);
		ArrayList<Album> albums = albumDal.getAlbumOnMDS();
		bundle = new Bundle();
		bundle.putParcelableArrayList(constants.ALBUMS_KEY, albums);
		albumFragment.setArguments(bundle);
		albumDal.close();

		PlaylistFragment playlistFragment = PlaylistFragment.newInstance();
		PlaylistDal playlistDal = new PlaylistDal(this);
		ArrayList<Playlist> playlists = playlistDal.getAllPlayList();
		bundle = new Bundle();
		bundle.putParcelableArrayList(constants.PLAYLISTS_KEY, playlists);
		playlistFragment.setArguments(bundle);
		playlistDal.close();

		HistoryFragment historyFragment = HistoryFragment.newInstance();

		fragments.add(historyFragment);
		fragments.add(downloadedFragment);
		fragments.add(trackFragment);
		fragments.add(artistFragment);
		fragments.add(albumFragment);
		fragments.add(playlistFragment);

		titles.add(getResources().getString(R.string.history_label));
		titles.add(getResources().getString(R.string.downloaded_label));
		titles.add(getResources().getString(R.string.track_label));
		titles.add(getResources().getString(R.string.artist_label));
		titles.add(getResources().getString(R.string.album_label));
		titles.add(getResources().getString(R.string.playlist_label));
		loadAd();
	}

	private class Receiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			if (action
					.equalsIgnoreCase(AudioDownloaderService.BROADCAST_ACTION)) {
				Bundle bundle = intent.getExtras();
				if (bundle.containsKey(AudioDownloaderService.DOWNLOAD_STATE)) {
					int code = bundle
							.getInt(AudioDownloaderService.DOWNLOAD_STATE);
					switch (code) {
					case AudioDownloaderService.OK_CODE:
						if (tabPagerAdapter != null) {
							HistoryFragment historyFragment = (HistoryFragment) tabPagerAdapter
									.getItem(0);
							historyFragment.refreshUI();
						}
						Toast ts = Toast.makeText(context, getResources()
								.getString(R.string.download_ok), 5000);
						ts.show();
						break;

					case AudioDownloaderService.NO_NETWORK_CODE:
						ts = Toast.makeText(
								context,
								getResources().getString(
										R.string.download_failed), 5000);
						ts.show();
						break;

					case AudioDownloaderService.FILE_NOT_FOUND_CODE:
						ts = Toast.makeText(
								context,
								getResources().getString(
										R.string.download_failed), 5000);
						ts.show();
						break;
					default:
						break;
					}
				}// end if
			}// end if
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.btn_action_remove:
			HistoryDal historyDal = new HistoryDal(ExplorationActivity.this);
			historyDal.getConnect();
			historyDal.removeAll();
			historyDal.close();
			if (tabPagerAdapter != null) {
				HistoryFragment historyFragment = (HistoryFragment) tabPagerAdapter
						.getItem(0);
				historyFragment.refreshUI();
			}
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.exploration_activity_layout);
		// Intent intent = new Intent(this, AudioPlayerService.class);
		// startService(intent);
		IntentFilter intentFilter = new IntentFilter(
				AudioDownloaderService.BROADCAST_ACTION);
		registerReceiver(receiver, intentFilter);
		setTabFragments();
		viewPager = (ViewPager) findViewById(R.id.pager);
		tabPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager(),
				fragments, titles);
		viewPager.setAdapter(tabPagerAdapter);
		disabledItems.clear();
		enableItems.add(Integer.valueOf(R.id.btn_action_remove));
		setEnableItem(enableItems);
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				enableItems.clear();
				disabledItems.add(Integer.valueOf(R.id.btn_action_add_playlist));
				// TODO Auto-generated method stub
				switch (position) {
				// history
				case 0:
					disabledItems.clear();
					enableItems.add(Integer.valueOf(R.id.btn_action_remove));
					setEnableItem(enableItems);
					break;

				// download folder
				case 1:
					break;

				// track
				case 2:
					break;

				// artirst
				case 3:
					break;

				// album
				case 4:
					break;

				// playlist
				case 5:
					disabledItems.clear();
					enableItems.add(Integer
							.valueOf(R.id.btn_action_add_playlist));
					setEnableItem(enableItems);
					break;

				default:
					break;
				}
				refreshActionBarMenu();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void savePlaylist(ArrayList<Track> tracks) {
		// TODO Auto-generated method stub
		PlaylistDal playlistDal = new PlaylistDal(getApplicationContext());
		int playlistId = playlistDal.addPlayList(this.playlistName);
		trackDal = new TrackDal(this);
		trackDal.getConnect();
		for (int i = 0; i < tracks.size(); i++) {
			trackDal.insertTrack(tracks.get(i), playlistId);
		}
		playlistDal.close();
		trackDal.close();
		if (tabPagerAdapter != null) {
			PlaylistFragment playlistFragment = (PlaylistFragment) tabPagerAdapter
					.getItem(5);
			Playlist playlist = new Playlist();
			playlist.setId(playlistId);
			playlist.setName(playlistName);
			playlistFragment.refreshViewAfterAddItem(playlist);
		}
	}

	@Override
	public void getInputTextValue(String inputText) {
		// TODO Auto-generated method stub
		this.playlistName = inputText;
		PlaylistDal playlistDal = new PlaylistDal(this);

		boolean hasExist = playlistDal.checkExistNamePlaylist(inputText);
		if (!hasExist) {
			CustomSelectTrackDialogFragment dialog = CustomSelectTrackDialogFragment
					.newInstance();
			dialog.show(getSupportFragmentManager(), "dialog");
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.exist_label));
			builder.setNegativeButton(
					getResources().getString(R.string.ok_label),
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

						}
					});
			builder.create().show();
		}
		playlistDal.close();
	}
}
