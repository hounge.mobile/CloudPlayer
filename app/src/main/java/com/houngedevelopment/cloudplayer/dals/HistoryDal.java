package com.houngedevelopment.cloudplayer.dals;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bk.lrandom.yourcloudplayer.models.Track;

public class HistoryDal {
	DB dbOpen;
	SQLiteDatabase dbHandler;
	Context context;
	ContentResolver contentResolver;

	public static final String COL_ID = "_id";
	public static final String COL_LIKE = "_likes";
	public static final String COL_TITLE = "_title";
	public static final String COL_REMOTE_ID = "_remote_id";
	public static final String TBL_HISTORY = "_tbl_history";
	public static final String COL_DOWNLOADED_FLAG = "_downloaded";
	public static final String COL_REMOTE_URL = "_remote_url";
	public static final int DOWNLOADED_FLAG = 1;
	public static final int PENDDING_FLAG = 0;

	public HistoryDal() {
		// TODO Auto-generated constructor stub
	}

	public HistoryDal(Context context) {
		super();
		this.context = context;
		this.contentResolver = context.getContentResolver();
	}

	public void getConnect() {
		dbOpen = new DB(context);
		dbHandler = dbOpen.getWritableDatabase();
	}

	// operation on own database
	public long insertTrack(Track track, int flag) {
		ContentValues values = new ContentValues();
		values.put(COL_REMOTE_ID, track.getRemote_id());
		values.put(COL_LIKE, track.getCountLike());
		values.put(COL_REMOTE_URL, track.getPath());
		values.put(COL_DOWNLOADED_FLAG, flag);
		values.put(COL_TITLE, track.getTitle());
		return dbHandler.insert(TBL_HISTORY, null, values);
	}

	public ArrayList<Track> getTracks() {
		String query = "SELECT * FROM " + TBL_HISTORY + " ORDER BY " + COL_ID
				+ " DESC";
		Cursor cursor = dbHandler.rawQuery(query, null);
		return convertOwnDBCursorToArrayList(cursor);
	}

	public ArrayList<Track> getTracksByID(int id) {
		String query = "SELECT * FROM " + TBL_HISTORY + " WHERE "
				+ COL_REMOTE_ID + "=" + id + " ORDER BY " + COL_ID + " DESC";
		Cursor cursor = dbHandler.rawQuery(query, null);
		return convertOwnDBCursorToArrayList(cursor);
	}

	public ArrayList<Track> getTracksDownloaded() {
		String query = "SELECT * FROM " + TBL_HISTORY + " WHERE "
				+ COL_DOWNLOADED_FLAG + "=" + DOWNLOADED_FLAG + " ORDER BY "
				+ COL_ID + " DESC";
		Cursor cursor = dbHandler.rawQuery(query, null);
		return convertOwnDBCursorToArrayList(cursor);
	}

	public ArrayList<Track> getTracksPendingDownload() {
		String query = "SELECT * FROM " + TBL_HISTORY + " WHERE "
				+ COL_DOWNLOADED_FLAG + "=" + PENDDING_FLAG + " ORDER BY "
				+ COL_ID + " DESC";
		Cursor cursor = dbHandler.rawQuery(query, null);
		return convertOwnDBCursorToArrayList(cursor);
	}

	public boolean removeTracksByID(int id) {
		int deleted = dbHandler.delete(TBL_HISTORY, COL_REMOTE_ID + "=" + id,
				null);
		if (deleted > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean removeAll() {
		int deleted = dbHandler.delete(TBL_HISTORY, null, null);
		if (deleted > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void updateFlagHistory(int flag, int id) {
		if (id != 0) {
			ContentValues values = new ContentValues();
			values.put(COL_DOWNLOADED_FLAG, flag);
			dbHandler.update(TBL_HISTORY, values, COL_ID + "=" + id, null);
		}
	}

	// end
	private ArrayList<Track> convertOwnDBCursorToArrayList(Cursor cursor) {
		ArrayList<Track> trackList = new ArrayList<Track>();
		if (cursor.moveToFirst() && cursor != null) {
			do {
				Track track = new Track();
				track.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
				track.setTitle(cursor.getString(cursor
						.getColumnIndex(COL_TITLE)));
				track.setPath(cursor.getString(cursor
						.getColumnIndex(COL_REMOTE_URL)));
				track.setCountLike(cursor.getInt(cursor
						.getColumnIndex(COL_LIKE)));
				track.setDownloadFlag(cursor.getInt(cursor
						.getColumnIndex(COL_DOWNLOADED_FLAG)));
				trackList.add(track);
			} while (cursor.moveToNext());
			cursor.close();
		}
		return trackList;
	}

	public void close() {
		if (dbHandler != null) {
			dbHandler.close();
		}
	}
}
