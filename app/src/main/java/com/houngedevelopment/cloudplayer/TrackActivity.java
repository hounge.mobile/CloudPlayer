package com.houngedevelopment.cloudplayer;

import java.util.ArrayList;

import com.houngedevelopment.cloudplayer.R;
import com.houngedevelopment.cloudplayer.business.Ultils;
import com.houngedevelopment.cloudplayer.conf.constants;
import com.houngedevelopment.cloudplayer.dals.ArtistDal;
import com.houngedevelopment.cloudplayer.dals.TrackDal;
import com.houngedevelopment.cloudplayer.models.Track;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class TrackActivity extends ActionBarParentActivity {
	ArtistDal artistDal;
	TrackDal trackDal;
	ListView list;
	ArrayList<Track> tracks;

	public void refreshUI(Track track) {
		tracks.remove(track);
		list.invalidateViews();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.track_context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.del_track_item:
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
			dialogBuilder.setMessage(getResources().getString(
					R.string.confirm_delete_audio_msg));
			dialogBuilder.setPositiveButton(
					getResources().getString(R.string.ok_label),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Boolean deleted = Ultils.removeTrack(
									getApplicationContext(),
									tracks.get(info.position));
							if (deleted) {
								Toast toast = Toast
										.makeText(
												getApplicationContext(),
												tracks.get(info.position)
														.getTitle()
														+ " "
														+ getResources()
																.getString(
																		R.string.file_deleted_result_msg),
												Integer.parseInt(getResources()
														.getString(
																R.string.toast_time_out)));
								toast.show();
								refreshUI(tracks.get(info.position));
							}
						}
					});
			dialogBuilder.setNegativeButton(
					getResources().getString(R.string.cancel_label),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
						}
					});
			dialogBuilder.create().show();
			break;

		case R.id.set_ringtone_item:
			Ultils.setRingtone(info.position, tracks.get(info.position), this);
			break;

		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview_container_layout);
		trackDal = new TrackDal(getApplicationContext());
		Bundle bundle = this.getIntent().getExtras();
		list = (ListView) findViewById(R.id.list);
		if (bundle != null) {
			if (bundle.getInt(constants.ARTIST_ID_KEY) != 0) {
				int id = bundle.getInt(constants.ARTIST_ID_KEY);
				tracks = trackDal.getTracksByArtistIdOnMDS(id);
				Ultils.sendTrackToPlayer(this, tracks, list);
				trackDal.close();
			}

			if (bundle.getInt(constants.ALBUM_ID_KEY) != 0) {
				int id = bundle.getInt(constants.ALBUM_ID_KEY);
				tracks = trackDal.getTracksByAlbumIdOnMDS(id);
				Ultils.sendTrackToPlayer(this, tracks, list);
				trackDal.close();
			}

			if (bundle.getInt(constants.PLAYLIST_ID_KEY) != 0) {
				int id = bundle.getInt(constants.PLAYLIST_ID_KEY);
				trackDal.getConnect();
				tracks = trackDal.getTracksByPlaylistID(id);
				Ultils.sendTrackToPlayer(this, tracks, list);
				trackDal.close();
			}
			registerForContextMenu(list);
		}
	}
}
