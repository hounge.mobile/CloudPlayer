package com.houngedevelopment.cloudplayer.adapter;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.models.Track;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class TrackSelectAdapter extends ArrayAdapter<Track> {
	private Context context;
	private int itemLayoutResource;
	private ArrayList<Track> tracks;

	public TrackSelectAdapter(Context context, int itemLayoutResource,
			ArrayList<Track> items) {
		super(context, itemLayoutResource, items);
		this.itemLayoutResource = itemLayoutResource;
		this.context = context;
		this.tracks = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(this.itemLayoutResource, null);
		}
		Track track = getItem(position);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(track.getTitle());
		TextView artist = (TextView) view.findViewById(R.id.artist);
		artist.setText(track.getArtist());
		CheckBox cbox = (CheckBox) view.findViewById(R.id.cbox);
		if (track.getChecked()) {
			cbox.setChecked(true);
		} else {
			cbox.setChecked(false);
		}
		return view;
	}
}