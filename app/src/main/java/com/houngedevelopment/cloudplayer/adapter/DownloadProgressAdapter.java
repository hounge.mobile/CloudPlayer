package com.houngedevelopment.cloudplayer.adapter;

import java.util.ArrayList;
import java.util.List;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.models.Track;
import com.bk.lrandom.yourcloudplayer.services.AudioDownloaderService;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

public class DownloadProgressAdapter extends ArrayAdapter<Track> implements
		OnClickListener {
	private Context context;
	private int itemLayoutResource;
	Spinner spinner;
	String[] menus;
	ArrayList<Track> tracks;

	public DownloadProgressAdapter(Context context, int itemLayoutResource,
			ArrayList<Track> items) {
		super(context, itemLayoutResource, items);
		this.itemLayoutResource = itemLayoutResource;
		this.context = context;
		this.tracks = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(this.itemLayoutResource, null);
		}
		Track track = getItem(position);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(track.getTitle());
		ImageButton btnCancel = (ImageButton) view.findViewById(R.id.btnState);
		btnCancel.setTag(Integer.valueOf(position));
		btnCancel.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ImageButton bntCancel = (ImageButton) v;
		int index = (Integer) v.getTag();
		Track track = tracks.get(index);
		View parent = (View) v.getParent();
		ProgressBar prg = (ProgressBar) parent.findViewById(R.id.download_prg);
		prg.setProgress(0);
		Intent intent = new Intent(context, AudioDownloaderService.class);
		intent.putExtra(AudioDownloaderService.STOP_TRACK_KEY, index);
		context.startService(intent);
		tracks.remove(index);
		notifyDataSetChanged();
	}
}