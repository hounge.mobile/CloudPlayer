package com.houngedevelopment.cloudplayer.adapter;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.models.Gender;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GendersAdapter extends ArrayAdapter<Gender> {
	private Context context;
	private int itemLayoutResource;

	public GendersAdapter(Context context, int itemLayoutResource,
			ArrayList<Gender> categories) {
		super(context, itemLayoutResource, categories);
		this.itemLayoutResource = itemLayoutResource;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(this.itemLayoutResource, null);
		}
		Gender categorie = getItem(position);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(categorie.getTitle());
		return view;
	}

}
