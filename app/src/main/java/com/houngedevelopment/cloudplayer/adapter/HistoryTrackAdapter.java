package com.houngedevelopment.cloudplayer.adapter;

import java.util.ArrayList;

import android.R.integer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import android.widget.Spinner;
import android.widget.TextView;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.dals.HistoryDal;
import com.bk.lrandom.yourcloudplayer.models.Track;
import com.bk.lrandom.yourcloudplayer.services.AudioDownloaderService;

public class HistoryTrackAdapter extends ArrayAdapter<Track> implements
		OnClickListener {
	private Context context;
	private int itemLayoutResource;
	Spinner spinner;
	String[] menus;
	ArrayList<Track> tracks;

	public HistoryTrackAdapter(Context context, int itemLayoutResource,
			ArrayList<Track> items) {
		super(context, itemLayoutResource, items);
		this.itemLayoutResource = itemLayoutResource;
		this.context = context;
		this.tracks = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(this.itemLayoutResource, null);
		}
		Track track = getItem(position);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(track.getTitle());
		ImageButton btnState = (ImageButton) view.findViewById(R.id.btnState);
		btnState.setTag(Integer.valueOf(position));
		if (track.getDownloadFlag() == HistoryDal.DOWNLOADED_FLAG) {
			btnState.setImageResource(R.drawable.ic_done);
		} else {
			btnState.setImageResource(R.drawable.ic_download_dark);
		}
		btnState.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ImageButton but = (ImageButton) v;
		// but.setImageResource(R.drawable.ic_launcher);
		int index = (Integer) v.getTag();
		Track track = tracks.get(index);
		int flag = track.getDownloadFlag();
		if (flag == HistoryDal.PENDDING_FLAG) {
			but.setVisibility(Button.INVISIBLE);
			Intent intent = new Intent(context, AudioDownloaderService.class);
			intent.putExtra(AudioDownloaderService.SEND_TRACK_KEY, track);
			context.startService(intent);
		}
	}
}
