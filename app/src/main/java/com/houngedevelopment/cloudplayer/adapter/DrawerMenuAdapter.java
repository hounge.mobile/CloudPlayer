package com.houngedevelopment.cloudplayer.adapter;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.models.DrawerMenuItem;
import com.koushikdutta.ion.Ion;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerMenuAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<DrawerMenuItem> drawerItems;

	public DrawerMenuAdapter(Context context,
			ArrayList<DrawerMenuItem> drawerItems) {
		this.context = context;
		this.drawerItems = drawerItems;
	}

	@Override
	public int getCount() {
		return drawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return drawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_menu_item, null);
		}
		final ImageView imgIcon = (ImageView) convertView
				.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		imgIcon.setImageResource(drawerItems.get(position).getIcon());
		txtTitle.setText(drawerItems.get(position).getTitle());
		return convertView;
	}

}