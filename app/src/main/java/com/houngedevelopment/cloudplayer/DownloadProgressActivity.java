package com.houngedevelopment.cloudplayer;

import java.util.ArrayList;

import com.bk.lrandom.yourcloudplayer.R;
import com.bk.lrandom.yourcloudplayer.adapter.DownloadProgressAdapter;
import com.bk.lrandom.yourcloudplayer.conf.constants;
import com.bk.lrandom.yourcloudplayer.models.Track;
import com.bk.lrandom.yourcloudplayer.services.AudioDownloaderService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.ProgressBar;

public class DownloadProgressActivity extends ActionBarActivity {
	ListView list;
	DownloadProgressAdapter adapter;
	Receiver receiver;
	Track track;
	ArrayList<Track> tracks = new ArrayList<Track>();

	private class Receiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			if (action
					.equalsIgnoreCase(AudioDownloaderService.BROADCAST_ACTION)) {
				if (intent.hasExtra(AudioDownloaderService.CURRENT_ID_KEY)
						&& intent.hasExtra(AudioDownloaderService.PERCENT_KEY)) {
					Track currentTrack = intent.getExtras().getParcelable(
							AudioDownloaderService.CURRENT_ID_KEY);
					if (currentTrack != null) {
						Log.i("CURRENT_ID", currentTrack.getRemote_id() + "");
						int current_index = getIndexByRemoteId(currentTrack
								.getRemote_id());
						if (list.getChildAt(current_index) != null) {
							ProgressBar downloadPrg = (ProgressBar) list
									.getChildAt(current_index).findViewById(
											R.id.download_prg);
							int downloadPercent = intent.getExtras().getInt(
									AudioDownloaderService.PERCENT_KEY);
							downloadPrg.setProgress(downloadPercent);
						}
					}
				}

				if (intent.hasExtra(AudioDownloaderService.DOWNLOAD_STATE)) {
					int code = intent.getExtras().getInt(
							AudioDownloaderService.DOWNLOAD_STATE);
				}

				if (intent.hasExtra(constants.TRACKS_KEY)) {
					tracks = intent.getExtras().getParcelableArrayList(
							constants.TRACKS_KEY);
					adapter = new DownloadProgressAdapter(
							DownloadProgressActivity.this,
							R.layout.download_track_item_layout, tracks);
					list.setAdapter(adapter);
				}
			}
		}
	}

	private int getIndexByRemoteId(int id) {
		for (int i = 0; i < tracks.size(); i++) {
			if (id == tracks.get(i).getRemote_id()) {
				return i;
			}
		}
		return -1;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview_container_layout);
		list = (ListView) findViewById(R.id.list);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setTitle(getResources().getString(
				R.string.download_in_progress));
		receiver = new Receiver();
		IntentFilter intentFilter = new IntentFilter(
				AudioDownloaderService.BROADCAST_ACTION);
		registerReceiver(receiver, intentFilter);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Intent intent = new Intent(this, AudioDownloaderService.class);
		intent.putExtra(AudioDownloaderService.GET_TRACKS_KEY, "ok");
		Log.i("SEND", "SEND");
		startService(intent);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// unregisterReceiver(receiver);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
