package com.houngedevelopment.cloudplayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

import com.houngedevelopment.cloudplayer.R;
import com.houngedevelopment.cloudplayer.adapter.TrackAdapter;
import com.houngedevelopment.cloudplayer.business.JSONFetchTask;
import com.houngedevelopment.cloudplayer.conf.constants;
import com.houngedevelopment.cloudplayer.fragments.ExploreStreamFragment;
import com.houngedevelopment.cloudplayer.models.Track;

public class SearchActivity extends ActionBarParentActivity implements
		OnScrollListener {
	ArrayList<Track> track_list = new ArrayList<Track>();
	ListView list;
	TrackAdapter adapter;
	String TAG = "ExploreStreamFragment";
	JSONFetchTask jsonFetchTask;
	static InputStream is = null;
	static JSONObject jObj = null;
	static String jsonString = "";
	String query = null, tmpQuery = null;
	int COUNT_ITEM_LOAD_MORE = 10;
	int offset = 0;
	ProgressBar loadMorePrg;
	boolean loadingMore = true;

	public static final ExploreStreamFragment newInstance() {
		// TODO Auto-generated constructor stub
		ExploreStreamFragment fragment = new ExploreStreamFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview_container_layout);
		list = (ListView) findViewById(R.id.list);
		query = getResources().getString(R.string.json_get_track)
				+ "&filter=all";
		handleIntent(getIntent());
		changeActionBarTitle(getResources().getString(R.string.search_hint));
		loadAd();
	}

	private void parse(JSONObject jsonObj, boolean append) {
		try {
			int id = jsonObj.getInt(Track.TAG_ID);
			String name = jsonObj.getString(Track.TAG_TITLE);
			JSONObject username = jsonObj.getJSONObject(Track.TAG_USER);
			String artist = username.getString(Track.TAG_ARTIST);
			String thumb = jsonObj.getString(Track.TAG_THUMB);
			String path = jsonObj.getString(Track.TAG_SC_PATH) + "?client_id="
					+ getResources().getString(R.string.soundcloud_client_id);
			String downloadable = jsonObj.getString(Track.TAG_DOWNLOADABLE);
			Track track = new Track();
			track.setRemote_id(id);
			track.setPath(path);
			track.setTitle(name);
			track.setArtist(artist);
			track.setThumb(thumb);
			track.setDownloadable(downloadable);
			if (downloadable.equals("true")) {
				String download_url = jsonObj.getString(Track.TAG_DOWNLOAD);
				track.setDownload_url(download_url);
			}
			if (append) {
				track_list.add(track);
			} else {
				track_list.add(0, track);
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.w(TAG, e.toString());
			loadingMore = false;
		}
	}

	private void parseAndAppend(String jsonString) {
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObj = jsonArray.getJSONObject(i);
				parse(jsonObj, true);
			}
			loadingMore = false;
			adapter.notifyDataSetChanged();
			loadMorePrg.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			loadingMore = true;
			loadMorePrg.setVisibility(View.GONE);
		}
	}

	private String feedJson(String pullQuery) {
		try {
			HttpGet httpGet = null;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			if (pullQuery != null && !pullQuery.equalsIgnoreCase("")) {
				httpGet = new HttpGet(pullQuery);
			} else {
				httpGet = new HttpGet(tmpQuery);
			}
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, constants.STREAM_READER_CHARSET), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			jsonString = sb.toString();
			Log.i("JSON_FETCH_TAG", jsonString);
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}
		;
		return jsonString.replaceAll("\\\\'", "'");
	}

	private class LoadMoreDataTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			if (isCancelled()) {
				return null;
			}
			return feedJson(null);
		}

		@Override
		protected void onPostExecute(String result) {
			parseAndAppend(jsonString);
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			loadMorePrg.setVisibility(View.VISIBLE);
			loadingMore = true;
		}

		@Override
		protected void onCancelled() {
			loadingMore = false;
			loadMorePrg.setVisibility(View.GONE);
		}
	}

	@SuppressLint("NewApi")
	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String title = intent.getStringExtra(SearchManager.QUERY);
			Log.i(TAG, title);
			try {
				title = URLEncoder.encode(title, "utf-8");
				Log.i(TAG, title);
				if (title != null && title != "") {
					query += "&q=" + title;
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		list = (ListView) findViewById(R.id.list);
		list.setOnItemClickListener(listViewOnClick);
		Bundle bundle = getIntent().getExtras();
		// if (bundle != null) {
		// if (bundle.containsKey(constants.TITLE_KEY)) {
		// String title = bundle.getString(constants.TITLE_KEY);
		// if (title != null && title != "") {
		// try {
		// title = URLEncoder.encode(title, "utf-8");
		// } catch (UnsupportedEncodingException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// query += "&title=" + title;
		// }
		// }
		// }
		adapter = new TrackAdapter(this, R.layout.sc_track_item_layout,
				track_list);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout footerView = (LinearLayout) inflater.inflate(
				R.layout.footer_loadmore_layout, null);
		loadMorePrg = (ProgressBar) footerView.findViewById(R.id.prgLoadMore);
		list.addFooterView(footerView);
		list.setAdapter(adapter);
		list.setOnScrollListener(this);

		tmpQuery = query + "&offset=" + offset + "&limit="
				+ COUNT_ITEM_LOAD_MORE;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			new LoadMoreDataTask()
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			new LoadMoreDataTask().execute();
		}
	}

	private OnItemClickListener listViewOnClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList(constants.TRACKS_KEY, track_list);
			bundle.putInt(constants.TRACK_INDEX_KEY, arg2);
			Intent intent = new Intent(SearchActivity.this,
					PlayerActivity.class);
			intent.putExtras(bundle);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
	};

	@SuppressLint("NewApi")
	@Override
	public void onScroll(AbsListView view, int offsetVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		int lastInScreen = offsetVisibleItem + visibleItemCount;
		if ((lastInScreen == totalItemCount) && !loadingMore) {
			offset += COUNT_ITEM_LOAD_MORE;
			tmpQuery += "&offset=" + offset + "&limit=" + COUNT_ITEM_LOAD_MORE;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new LoadMoreDataTask()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				new LoadMoreDataTask().execute();
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

}
