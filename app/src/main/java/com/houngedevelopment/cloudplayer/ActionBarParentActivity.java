package com.houngedevelopment.cloudplayer;

import java.util.Iterator;
import java.util.LinkedHashSet;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.houngedevelopment.cloudplayer.fragments.CustomPromptDialogFragment;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class ActionBarParentActivity extends
		android.support.v7.app.ActionBarActivity {
	private LinkedHashSet<Integer> enableItems = new LinkedHashSet<Integer>();
	private LinkedHashSet<Integer> disableItems = new LinkedHashSet<Integer>();
	private Iterator<Integer> iter;
	private Tracker mTracker;
	String[] searchMode = null;

	public void setEnableItem(LinkedHashSet<Integer> items) {
		this.enableItems = items;
	}

	public void setDisableItem(LinkedHashSet<Integer> items) {
		this.disableItems = items;
	}

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu
				.findItem(R.id.btn_action_search));
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		return true;
	}

	@Override
	public void startActivity(Intent intent) {
		// TODO Auto-generated method stub
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

		}
		super.startActivity(intent);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (!disableItems.isEmpty()) {
			iter = disableItems.iterator();
			while (iter.hasNext()) {
				MenuItem item = menu.findItem(iter.next());
				item.setVisible(false);
			}
		}

		if (!enableItems.isEmpty()) {
			iter = enableItems.iterator();
			while (iter.hasNext()) {
				MenuItem item = menu.findItem(iter.next());
				item.setVisible(true);
			}
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpTo(this, new Intent(this, HomeActivity.class));
			break;

		case R.id.btn_action_add_playlist:
			CustomPromptDialogFragment newFragment = CustomPromptDialogFragment
					.newInstance(getResources().getString(R.string.name_label));
			newFragment.show(getSupportFragmentManager(), "dialog");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void refreshActionBarMenu() {
		this.supportInvalidateOptionsMenu();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);

	}

	public void showDialog(String msg) {
		AlertDialog.Builder buidler = new AlertDialog.Builder(
				ActionBarParentActivity.this);
		buidler.setMessage(msg);
		buidler.setPositiveButton(getResources().getString(R.string.ok_label),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method
						// stub

					}
				});
		AlertDialog dialog = buidler.create();
		dialog.show();
	}

	public void changeActionBarTitle(String title) {
		getSupportActionBar().setTitle(title);
	}

	public void showMsg(String msg) {
		Toast ts = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		ts.show();
	}

	public void loadAd() {
		AdView adView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice(
						getResources()
								.getString(R.string.admob_test_device_ids))
				.build();
		adView.loadAd(adRequest);
	}

}
